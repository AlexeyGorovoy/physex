package com.teremok.physex.util;

import com.teremok.framework.util.ResourceManagerImpl;
import com.teremok.physex.PhysexGame;

/**
 * Created by Алексей on 09.07.2014
 */
public class PhysexResourceManager extends ResourceManagerImpl <PhysexGame> {
    public PhysexResourceManager(PhysexGame game) {
        super(game);
    }

    @Override
    public void preload() {
        super.preload();
    }
}

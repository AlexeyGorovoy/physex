package com.teremok.physex;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.teremok.framework.TeremokGame;
import com.teremok.framework.util.Animation;
import com.teremok.framework.util.Localizator;
import com.teremok.physex.screen.PhysexScreenController;
import com.teremok.physex.screen.Screens;
import com.teremok.physex.util.PhysexResourceManager;

public class PhysexGame extends TeremokGame {

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        resourceManager = new PhysexResourceManager(this);
        screenController = new PhysexScreenController(this);

        resourceManager.preload();
        resourceManager.finishLoading();

        Localizator.init(this);

        Animation.DURATION_NORMAL = 0.0f;
        Animation.DURATION_SHORT = 0.0f;
        Animation.DURATION_LONG = 0.0f;

        screenController.setScreen(Screens.MAIN);
    }
}

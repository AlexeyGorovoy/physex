package com.teremok.physex.tween;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.teremok.framework.ui.TexturePanel;

/**
 * Created by Алексей on 16.07.2014
 */
public class CentredTexturePanel extends TexturePanel {
    public CentredTexturePanel(TextureRegion region, float x, float y) {
        super(region, x, y);
        float width = region.getRegionWidth();
        float height = region.getRegionHeight();
        image.setOrigin(width/2, height/2);
        setWidth(width);
        setHeight(height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        float resultAlpha = parentAlpha < getColor().a ? parentAlpha : getColor().a;
        batch.setColor(getColor());
        image.setColor(getColor());
        image.setRotation(getRotation());
        image.draw(batch, resultAlpha);
    }


}

package com.teremok.physex.game.figures;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Алексей on 10.07.2014
 */
public class Circle extends Figure {
    static final float DENSITY = 1200f;
    static final float FRICTION = 0.5f;
    static final float RESTITUTION = 0.6f;

    public float radius;

    public Circle(World world, float x, float y, float radius) {
        this.radius = radius;
        this.x = x;
        this.y = y;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(x, y);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(radius);
        fixture = body.createFixture(circleShape, DENSITY);
        fixture.setFriction(FRICTION);
        fixture.setRestitution(RESTITUTION);

        Gdx.app.debug(getClass().getSimpleName(), "Create circle with mass : " + body.getMass());

        circleShape.dispose();
    }
}

package com.teremok.physex.game.figures;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Алексей on 10.07.2014
 */
public class Hazard extends Figure {

    static final float DENSITY = 3.0f;
    static final float FRICTION = 1.0f;

    public float width, height;

    public Hazard(World world, float x, float y, float width, float height) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(x + width/2, y + height/2);
        body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(width/2, height/2);
        fixture = body.createFixture(polygonShape, DENSITY);
        fixture.setFriction(FRICTION);

        polygonShape.dispose();
    }

    @Override
    public void update() {
    }
}

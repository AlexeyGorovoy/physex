package com.teremok.physex.game.figures;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Алексей on 11.07.2014
 */
public class Triangle extends Figure {
    static final float DENSITY = 11350f;
    static final float FRICTION = 0.43f;
    static final float RESTITUTION = 0.0f;

    public float side;

    public Triangle(World world, float x, float y, float side) {
        this.side = side;
        this.x = x;
        this.y = y;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(x, y);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(side/2, side/2);
        fixture = body.createFixture(polygonShape, DENSITY);
        fixture.setFriction(FRICTION);
        fixture.setRestitution(RESTITUTION);

        Gdx.app.debug(getClass().getSimpleName(), "Create square with mass : " + body.getMass());

        polygonShape.dispose();
    }

    @Override
    public void update() {
        x = body.getPosition().x-side/2;
        y = body.getPosition().y-side/2;
    }
}

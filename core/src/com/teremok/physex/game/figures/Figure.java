package com.teremok.physex.game.figures;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

/**
 * Created by Алексей on 11.07.2014
 */
public abstract class Figure {

    public float x;
    public float y;

    public Body body;
    public Fixture fixture;

    public void update() {
        x = body.getPosition().x;
        y = body.getPosition().y;
    }
}

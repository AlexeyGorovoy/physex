package com.teremok.physex.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.teremok.physex.game.figures.Circle;
import com.teremok.physex.game.figures.Hazard;
import com.teremok.physex.game.figures.Square;

import java.util.LinkedList;

/**
 * Created by Алексей on 10.07.2014
 */
public class PhysWorld {

    public static final float WIDTH = 8f;
    public static final float HEIGHT = 6f;

    private static float CIRCLE_RADIUS = 0.20f;

    private static float SQUARE_SIDE = 0.40f;

    World world;

    Hazard[] hazards;
    LinkedList<Circle> circles;
    LinkedList<Square> squares;

    public PhysWorld() {
        world = new World(new Vector2(0.0f, -9.81f), true);

        hazards = new Hazard[3];

        hazards[0] = new Hazard(world, 0, 0, 0.4f, 4.0f);
        hazards[1] = new Hazard(world, 7.6f, 0, 0.4f, 4.0f);
        hazards[2] = new Hazard(world, 0, 0, 8.0f, 0.4f);

        circles = new LinkedList<>();
        addCircle(4, 5, CIRCLE_RADIUS);

        squares = new LinkedList<>();
        addSquare(4.20f, 5.40f, SQUARE_SIDE);
    }

    public void update() {
        world.step(1 / 60f, 10, 10);
        updateCircles();
        updateSquares();
    }

    private void updateCircles() {
        for (Circle circle : circles) {
            circle.update();
        }
    }

    private void updateSquares() {
        for (Square square : squares) {
            square.update();
        }
    }

    public void addCircle(float x, float y, float radius) {
        circles.add(new Circle(world, x, y, radius));
    }

    public void addSquare(float x, float y, float radius) {
        squares.add(new Square(world, x, y, radius));
    }
}

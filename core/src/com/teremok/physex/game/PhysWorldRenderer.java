package com.teremok.physex.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.teremok.framework.screen.AbstractScreen;
import com.teremok.physex.game.figures.Circle;
import com.teremok.physex.game.figures.Hazard;
import com.teremok.physex.game.figures.Square;

/**
 * Created by Алексей on 10.07.2014
 */
public class PhysWorldRenderer extends Actor {

    private PhysWorld world;

    private float ppuY = AbstractScreen.HEIGHT / PhysWorld.HEIGHT;
    private float ppuX = AbstractScreen.WIDTH / PhysWorld.WIDTH;

    private ShapeRenderer renderer;

    public PhysWorldRenderer(PhysWorld world) {
        this.world = world;
        this.renderer = new ShapeRenderer();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.end();
        renderer.setProjectionMatrix(batch.getProjectionMatrix());
        renderer.setTransformMatrix(batch.getTransformMatrix());

        renderer.begin(ShapeRenderer.ShapeType.Filled);

        drawHazards();
        drawCircles();
        drawSquares();

        renderer.end();
        batch.begin();
    }


    private void drawHazards() {
        renderer.setColor(Color.BLUE);
        for (Hazard hazard : world.hazards) {
            renderer.rect(hazard.x*ppuX, hazard.y*ppuY, hazard.width*ppuX, hazard.height*ppuY);
        }
    }

    private void drawCircles() {
        renderer.setColor(Color.RED);
        for (Circle circle : world.circles) {
            renderer.circle(circle.x * ppuX, circle.y * ppuY, circle.radius * ppuX);
        }
    }

    private void drawSquares() {
        renderer.setColor(Color.GREEN);
        for (Square square : world.squares) {
            renderer.rect(  square.x *ppuX, square.y * ppuY, square.side * ppuX, square.side *ppuY);
        }
    }
}

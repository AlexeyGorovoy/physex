package com.teremok.physex.screen;

/**
 * Created by Алексей on 09.07.2014
 */
public interface Screens {
    public static final String GAME = "gameScreen";
    public static final String MAIN = "mainScreen";
    public static final String TWEEN = "tweenScreen";
}

package com.teremok.physex.screen;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.teremok.framework.screen.StaticScreen;
import com.teremok.framework.ui.Button;
import com.teremok.framework.ui.ButtonTexture;
import com.teremok.physex.PhysexGame;

/**
 * Created by Алексей on 09.07.2014
 */
public class MainScreen extends StaticScreen<PhysexGame> {

    private static final String START = "start";
    private static final String TWEEN = "tween";

    public MainScreen(PhysexGame game, String filename) {
        super(game, filename);
    }

    @Override
    protected void addActors() {
        ButtonTexture start = new ButtonTexture(uiElements.get(START));
        ButtonTexture tween = new ButtonTexture(uiElements.get(TWEEN));
        stage.addActor(start);
        stage.addActor(tween);
    }

    @Override
    protected void addListeners() {
        stage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return stage.hit(x,y,true) instanceof Button;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                switch (((Button)event.getTarget()).getCode()) {
                    case START:
                        screenController.setScreen(Screens.GAME);
                        break;
                    case TWEEN:
                        screenController.setScreen(Screens.TWEEN);
                        break;
                }
            }

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE) {
                    screenController.gracefullyExitGame();
                }
                return true;
            }
        });
    }
}

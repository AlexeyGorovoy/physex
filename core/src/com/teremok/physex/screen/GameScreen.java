package com.teremok.physex.screen;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.teremok.framework.screen.StaticScreen;
import com.teremok.physex.PhysexGame;
import com.teremok.physex.game.PhysWorld;
import com.teremok.physex.game.PhysWorldRenderer;

import java.util.Random;

/**
 * Created by Алексей on 09.07.2014
 */
public class GameScreen extends StaticScreen <PhysexGame> {

    PhysWorld world;
    PhysWorldRenderer renderer;

    Random random;

    public GameScreen(PhysexGame game, String filename) {
        super(game, filename);
        world = new PhysWorld();
        renderer = new PhysWorldRenderer(world);
        random = new Random();
    }

    @Override
    protected void addActors() {
        stage.addActor(renderer);
    }

    @Override
    protected void addListeners() {
         stage.addListener(new InputListener() {
             public boolean keyDown(InputEvent event, int keycode) {
                 if (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE) {
                     screenController.setScreen(Screens.MAIN);
                 }
                 return true;
             }

             @Override
             public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                 return true;
             }

             @Override
             public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                 if (button == Input.Buttons.LEFT)
                     world.addCircle(x/100, y/100, 0.10f+random.nextFloat()*0.3f);
                 if (button == Input.Buttons.RIGHT)
                     world.addSquare(x/100, y/100, 0.20f+random.nextFloat()*0.3f);
             }
         });
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        world.update();
    }
}

package com.teremok.physex.screen;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Back;
import aurelienribon.tweenengine.equations.Bounce;
import aurelienribon.tweenengine.equations.Cubic;
import aurelienribon.tweenengine.equations.Linear;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.teremok.framework.screen.StaticScreen;
import com.teremok.framework.tween.ActorTweenAccessor;
import com.teremok.framework.ui.TexturePanel;
import com.teremok.physex.PhysexGame;
import com.teremok.physex.tween.CentredTexturePanel;

/**
 * Created by Алексей on 11.07.2014
 */
public class TweenScreen extends StaticScreen<PhysexGame> {

    private TweenManager manager;

    public TweenScreen(PhysexGame game, String filename) {
        super(game, filename);
        Tween.registerAccessor(Actor.class, new ActorTweenAccessor());
        manager = new TweenManager();
    }

    @Override
    protected void addActors() {
        addVariant3();
    }

    @Override
    protected void addListeners() {
        stage.addListener(new InputListener() {
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE) {
                    screenController.setScreen(Screens.MAIN);
                }
                return true;
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            }
        });
    }

    private void addVariant1() {

        TexturePanel pane64Left = new CentredTexturePanel(atlas.findRegion("64"), -200-32, 300);
        stage.addActor(pane64Left);

        Tween.to(pane64Left, ActorTweenAccessor.POSITION_X, 1.0f).delay(0.5f).ease(Back.OUT).target(300-32).start(manager);
        Tween.to(pane64Left, ActorTweenAccessor.ROTATION, 1.0f).ease(Linear.INOUT).target(-360).start(manager);

        TexturePanel pane64Right = new CentredTexturePanel(atlas.findRegion("64"), 956-32, 300);
        stage.addActor(pane64Right);

        Tween.to(pane64Right, ActorTweenAccessor.POSITION_X, 1.0f).delay(0.5f).ease(Back.OUT).target(500-32).start(manager);
        Tween.to(pane64Right, ActorTweenAccessor.ROTATION, 1.0f).ease(Linear.INOUT).target(360).start(manager);

        TexturePanel pane64Center = new CentredTexturePanel(atlas.findRegion("64"), 400-32, 800);
        stage.addActor(pane64Center);

        Tween.to(pane64Center, ActorTweenAccessor.POSITION_Y, 1.0f).ease(Bounce.OUT).target(300).start(manager);
    }

    private void addVariant2() {

        TexturePanel pane64Left = new CentredTexturePanel(atlas.findRegion("64"), -200-32, 300);
        stage.addActor(pane64Left);

        Tween.to(pane64Left, ActorTweenAccessor.POSITION_X, 1.0f).delay(0.0f).ease(Cubic.OUT).target(256-32)
                .repeatYoyo(1, 2.0f).start(manager);

        TexturePanel pane64Right = new CentredTexturePanel(atlas.findRegion("64"),  -200-32, 300);
        stage.addActor(pane64Right);

        Tween.to(pane64Right, ActorTweenAccessor.POSITION_X, 1.0f).delay(0.0f).ease(Cubic.OUT).target(544-32)
                .repeatYoyo(1, 2.0f).start(manager);

        TexturePanel pane64Center = new CentredTexturePanel(atlas.findRegion("64"),  -200-32, 300);
        stage.addActor(pane64Center);

        Tween.to(pane64Center, ActorTweenAccessor.POSITION_X, 1.0f).ease(Cubic.OUT).target(400-32)
                .repeatYoyo(1, 2.0f).start(manager);
    }

    private void addVariant3() {

        TexturePanel pane64Left = new CentredTexturePanel(atlas.findRegion("64"), -200-32, 600);
        stage.addActor(pane64Left);

        Tween.to(pane64Left, ActorTweenAccessor.POSITION_Y, 1.0f).delay(0.0f).ease(Bounce.OUT).target(300)
                .repeatYoyo(1, 2.0f).start(manager);
        Tween.to(pane64Left, ActorTweenAccessor.POSITION_X, 1.0f).delay(0.0f).ease(Cubic.OUT).target(256-32)
                .repeatYoyo(1, 2.0f).start(manager);

        TexturePanel pane64Right = new CentredTexturePanel(atlas.findRegion("64"),  -200-32, 600);
        stage.addActor(pane64Right);

        Tween.to(pane64Right, ActorTweenAccessor.POSITION_Y, 1.0f).delay(0.0f).ease(Bounce.OUT).target(300)
                .repeatYoyo(1, 2.0f).start(manager);
        Tween.to(pane64Right, ActorTweenAccessor.POSITION_X, 1.0f).delay(0.0f).ease(Cubic.OUT).target(544-32)
                .repeatYoyo(1, 2.0f).start(manager);

        TexturePanel pane64Center = new CentredTexturePanel(atlas.findRegion("64"),  -200-32, 600);
        stage.addActor(pane64Center);

        Tween.to(pane64Center, ActorTweenAccessor.POSITION_Y, 1.0f).delay(0.0f).ease(Bounce.OUT).target(300)
                .repeatYoyo(1, 2.0f).start(manager);
        Tween.to(pane64Center, ActorTweenAccessor.POSITION_X, 1.0f).ease(Cubic.OUT).target(400-32)
                .repeatYoyo(1, 2.0f).start(manager);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        manager.update(delta);
    }
}

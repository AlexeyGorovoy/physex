package com.teremok.physex.screen;

import com.teremok.framework.screen.ScreenControllerImpl;
import com.teremok.framework.screen.StaticScreen;
import com.teremok.physex.PhysexGame;

/**
 * Created by Алексей on 09.07.2014
 */
public class PhysexScreenController extends ScreenControllerImpl <PhysexGame> {
    public PhysexScreenController(PhysexGame game) {
        super(game);
    }

    @Override
    public StaticScreen resolve(String s) {
        StaticScreen screen = null;

        switch (s) {
            case Screens.GAME:
                screen = new GameScreen(game, Screens.GAME);
                break;
            case Screens.MAIN:
                screen = new MainScreen(game, Screens.MAIN);
                break;
            case Screens.TWEEN:
                screen = new TweenScreen(game, Screens.TWEEN);
                break;
        }

        return screen;
    }
}
